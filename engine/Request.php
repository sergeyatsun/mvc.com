<?php


class Request 
{
    private $get;
    private $post;
    private $files;
    public function __construct() 
    {
        
        $this->get = $_GET;
        $this->post = $_POST;
        $this->files = $_FILES;
    }
    
    public function get($type, $value) {
        $result = $this->$type;
        if(isset($result[$value])){
            return $result[$value];
        }
        
        return FALSE;
    }

}