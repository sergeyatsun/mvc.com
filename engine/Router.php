<?php

final class Router {
    private $Request;
    private $route;
    private $path;
    private $params;
    private $routes; //4
    private $Controller; //7
    private $action; //8
    
    
    

    public function __construct(Request $Request) {
        
        $this->Request = $Request;
        $this->routes = ROUTES; //5
    
               
    }
    
    public function initRoute() 
    {
     
        $this->path = $this->Request->get('get', 'route'); //2
  
        if($this->path && $this->setRoute())
        {
            include $this->route['path'];   
            
            $this->Controller = new $this->route['controller']();
        
            $action = $this->route['action'];
            
            $this->Controller-> $action();
            
                    
            
          
        }elseif ($this->path && !$this->route)
        {
          echo 'show 404';
        }else 
        {
            echo 'show home page';   
        }
   
    }
   
    
    private function setRoute() 
    {
        $this->params= explode('/', $this->path);
        
        
            if ($this->routes[$this->path])
            {
                $this->route = $this->routes[$this->path];
                return TRUE;
            }
            foreach ($this->params as $key => $param) 
            {
                
             if($key)
             {
                 $name = implode('/', $this-> params);
             } else 
             {
                 array_pop($this->params); 
                 $name = implode('/', $this->params);
             } 
              if($this->routes[$name])
              {
                  $this->route = $this->routes[$name];   break;
              }    
             
            } 
            return $this->route ? true : false;
    } 
    
    
}