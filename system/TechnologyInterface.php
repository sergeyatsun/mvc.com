<?php

    interface TechnologyInterface {
        
        public function powerOn();
        
        public function powerOff();
}