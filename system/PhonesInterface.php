<?php
include './../system/TechnologyInterface.php';

    interface PhonesInterface extends TechnologyInterface
{
    const DISPLAY = '4k';
            
    public function setCall();
    public function getCall();
    public function sendSMS(String $message, Int $Num);
    public function getSMS();
    public function getMMS();
    
}