<?php

const ROUTES= 
[
      'home' => 
    [
        'controller' => 'HomeController',
        'action' => 'index',
    ],
    
    'page' => 
    [
        'controller' => 'PageController',
        'path' => '../controller/PageController.php',
        'action' => 'index',
        'slug' => 
        [
            'reg' => '^[0-9a-zA-Z-]{2,25}$'
        ]   
    ],
    
    'page/add' => 
    [
        'controller' => 'PageController',
        'path' => '../controller/PageController.php',
        'action' => 'add',
        'slug' => 
        [
            'reg' => '^[0-9a-zA-Z-]{2,25}$'
        ]   
    ],
  
    'registration' => 
    [
        'controller' => 'RegistrationController',
        'action' => 'index',    
    ],
    
    'login' => 
    [
        'controller' => 'LoginController',
        'action' => 'index',
    ]
    
];

